package ru.t1.dkononov.tm.listener.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.ProjectUpdateByIndexRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIndexListener extends AbstractProjectListener {

    @Getter
    @NotNull
    public final String NAME = "project-update-by-index";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Обновить проект по индексу.";

    @Override
    @EventListener(condition = "@projectUpdateByIndexListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[ENTER INDEX]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER NAME]");
        @NotNull final String name = TerminalUtil.inLine();
        System.out.println("[ENTER DESCRIPTION]");
        @NotNull final String description = TerminalUtil.inLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(getToken());
        request.setIndex(index);
        request.setDescription(description);
        request.setName(name);
        getProjectEndpoint().updateProjectByIndex(request);
    }

}
