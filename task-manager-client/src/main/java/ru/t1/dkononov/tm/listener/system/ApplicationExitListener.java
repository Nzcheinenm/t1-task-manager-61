package ru.t1.dkononov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.event.ConsoleEvent;

@Component
public final class ApplicationExitListener extends AbstractSystemListener {

    @Getter
    @NotNull
    public final String DESCRIPTION = "Закрыть приложение";

    @Getter
    @NotNull
    public final String NAME = "exit";

    @Nullable
    @Override
    public String getARGUMENT() {
        return null;
    }


    @Override
    @EventListener(condition = "@applicationExitListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.exit(0);
    }

}
