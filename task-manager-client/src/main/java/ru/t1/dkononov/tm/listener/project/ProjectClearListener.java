package ru.t1.dkononov.tm.listener.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.ProjectClearRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.exception.AbstractException;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @Getter
    @NotNull
    public final String NAME = "project-clear";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Очистить список проектов.";

    @Override
    @EventListener(condition = "@projectClearListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[PROJECT_CLEAR]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        getProjectEndpoint().clearProject(request);
    }

}
