package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String NAME = "task-remove-by-id";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Удалить задачу по Id.";

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[ENTER ID]");
        @NotNull final String value = TerminalUtil.inLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setId(value);
        getTaskEndpointClient().removeTaskById(request);
    }

}
