package ru.t1.dkononov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.model.ICommand;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.listener.AbstractListener;

import java.util.Arrays;
import java.util.Collection;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @Getter
    @NotNull
    public final String DESCRIPTION = "Показать список команд";

    @Getter
    @Autowired
    private AbstractListener[] listeners;

    @Getter
    @NotNull
    public final String NAME = "commands";

    @Getter
    @NotNull
    public final String ARGUMENT = "-cmd";

    @Override
    @EventListener(condition = "@commandListListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @Nullable final Collection<AbstractListener> commands = Arrays.asList(listeners);
        for (@NotNull final ICommand command : commands) {
            @NotNull final String name = command.getNAME();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
