package ru.t1.dkononov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkononov.tm.listener.AbstractListener;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractListener command);

    @Nullable
    AbstractListener getCommandByArgument(@Nullable String argument);

    @Nullable
    AbstractListener getCommandByName(@Nullable String name);

    @Nullable
    Collection<AbstractListener> getTerminalCommands();

    @Nullable
    Iterable<AbstractListener> getCommandsByArgument();

}
