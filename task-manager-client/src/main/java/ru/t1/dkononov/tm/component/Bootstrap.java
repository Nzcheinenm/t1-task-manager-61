package ru.t1.dkononov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.endpoint.*;
import ru.t1.dkononov.tm.api.services.ILoggerService;
import ru.t1.dkononov.tm.api.services.IPropertyService;
import ru.t1.dkononov.tm.api.services.IServiceLocator;
import ru.t1.dkononov.tm.api.services.ITokenService;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.listener.AbstractListener;
import ru.t1.dkononov.tm.util.SystemUtil;
import ru.t1.dkononov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMAND = "ru.t1.dkononov.tm.command";

    @NotNull
    @Autowired
    private AbstractListener[] abstractListeners;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);


    public void run(@NotNull final String[] args) {
        init();
        @NotNull String command = readCommand();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                if (command.isEmpty()) {
                    command = readCommand();
                    continue;
                }
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
            command = readCommand();
        }
    }

    private String readCommand() {
        return TerminalUtil.inLine();
    }

    private void init() {
        try {
            prepareStart();
        } catch (final Exception e) {
            loggerService.error(e);
            System.err.println("[INIT FAIL]");
        }
    }

    private void prepareStart() throws Exception {
        initPID();
        initLogger();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.init();
    }

    private void prepareShutdown() {
        fileScanner.stop();
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    private void initPID() throws IOException {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
    }


}
