package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.model.TaskDTO;
import ru.t1.dkononov.tm.dto.request.TaskListRequest;
import ru.t1.dkononov.tm.dto.response.TaskListResponse;
import ru.t1.dkononov.tm.enumerated.CustomSort;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String NAME = "task-list";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Вывести список задач.";

    @Override
    @EventListener(condition = "@taskListListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW TASKS]");
        System.out.println("[ENTER SORT: ]");
        System.out.println(Arrays.toString(CustomSort.values()));
        @NotNull final String sortType = TerminalUtil.inLine();
        @Nullable final CustomSort customSort = CustomSort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setCustomSort(customSort);
        @NotNull final TaskListResponse response = getTaskEndpointClient().listTask(request);
        if (response.getTasks() == null) response.setTasks(Collections.emptyList());
        @NotNull final List<TaskDTO> tasks = response.getTasks();
        showTasks(tasks);
    }

    public void showTasks(@NotNull final List<TaskDTO> tasks) {
        int index = 1;
        for (@Nullable final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + show(task));
            index++;
        }
    }

    @NotNull
    public String show(@NotNull final TaskDTO task) {
        return "[ID: " + task.getId() + "]\n" +
                "[NAME: " + task.getName() + "]\n" +
                "[DESC: " + task.getDescription() + "]\n" +
                "[STATUS: " + task.getStatus() + "]";
    }

}
