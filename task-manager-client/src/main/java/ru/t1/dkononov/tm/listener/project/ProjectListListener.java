package ru.t1.dkononov.tm.listener.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.model.ProjectDTO;
import ru.t1.dkononov.tm.dto.request.ProjectListRequest;
import ru.t1.dkononov.tm.dto.response.ProjectListResponse;
import ru.t1.dkononov.tm.enumerated.CustomSort;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @Getter
    @NotNull
    public final String NAME = "project-list";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Вывести список проектов.";


    @Override
    @EventListener(condition = "@projectListListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("[ENTER SORT: ]");
        System.out.println(Arrays.toString(CustomSort.values()));
        @NotNull final String sortType = TerminalUtil.inLine();
        @Nullable final CustomSort customSort = CustomSort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setCustomSort(customSort);
        int index = 0;
        @NotNull final ProjectListResponse response = getProjectEndpoint().listProject(request);
        if (response.getProjects() == null) response.setProjects(Collections.emptyList());
        @NotNull final List<ProjectDTO> projects = response.getProjects();
        for (@NotNull final ProjectDTO project : projects) {
            index++;
            System.out.println(index + ". " + project.getName());
        }
    }

}
