package ru.t1.dkononov.tm.listener.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkononov.tm.api.endpoint.IUserEndpoint;
import ru.t1.dkononov.tm.dto.model.UserDTO;
import ru.t1.dkononov.tm.exception.field.UserNotFoundException;
import ru.t1.dkononov.tm.listener.AbstractListener;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @Getter
    @Setter
    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @Getter
    @Setter
    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    protected void showUser(@Nullable final UserDTO user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Nullable
    @Override
    public String getARGUMENT() {
        return null;
    }
}
