package ru.t1.dkononov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.services.model.IUserOwnedService;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.IdEmptyException;
import ru.t1.dkononov.tm.exception.field.IndexIncorrectException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.model.AbstractUserOwnedModel;
import ru.t1.dkononov.tm.repository.model.AbstractUserOwnedRepository;
import ru.t1.dkononov.tm.repository.model.UserRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel>
        extends AbstractService<M> implements IUserOwnedService<M> {

    @NotNull
    @Autowired
    protected AbstractUserOwnedRepository<M> repository;

    @NotNull
    @Getter
    @Autowired
    protected UserRepository userRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        repository.findAll().stream()
                .filter(x -> x.getUser().getId().equals(userId))
                .forEach(x -> repository.delete(x));
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.findAll().stream().filter(x -> x.getId().equals(id) && x.getUser().getId().equals(userId)).findFirst().get() != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Sort customSort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (customSort == null) return findAll(userId);
        return findAll(userId, customSort.ascending());
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public M add(@Nullable final String userId, @Nullable final M model)
            throws ProjectNotFoundException, UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final M result;
        repository.save(model);
        result = findById(userId, model.getId());
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Collection<M> add(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::save);
        return models;
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::saveAndFlush);
        return models;
    }


    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public M remove(@NotNull final String userId, @Nullable final M model) throws UserIdEmptyException {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        repository.delete(repository.findAll().stream().filter(x -> x.getUser().getId().equals(userId)).findFirst().get());
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findAll()
                .stream()
                .filter(x -> x.getId().equals(id) && x.getUser().getId().equals(userId))
                .findFirst().get();
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findAll().get(0);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M result;
        result = repository.findAll()
                .stream()
                .filter(x -> x.getId().equals(id) && x.getUser().getId().equals(userId))
                .findFirst().get();
        remove(userId, result);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final M result;
        result = repository.findAll().get(0);
        repository.delete(
                repository.findAll()
                        .stream()
                        .filter(x -> x.getUser().getId().equals(userId)).findFirst()
                        .get()
        );
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.findAll().stream()
                .filter(x -> x.getUser().getId().equals(userId))
                .forEach(x -> repository.delete(x));
    }

}
