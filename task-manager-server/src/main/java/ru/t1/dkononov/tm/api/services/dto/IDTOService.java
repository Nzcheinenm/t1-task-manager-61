package ru.t1.dkononov.tm.api.services.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkononov.tm.dto.model.AbstractModelDTO;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface IDTOService<M extends AbstractModelDTO> {
    @SneakyThrows
    void clear();

    @SneakyThrows
    boolean existsById(@Nullable String id);

    @NotNull
    @SneakyThrows
    List<M> findAll();

    @SneakyThrows
    @Nullable
    Optional<M> add(M model);

    @Nullable
    @SneakyThrows
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    @SneakyThrows
    Collection<M> set(@NotNull Collection<M> models);

    @SneakyThrows
    @Nullable
    Optional<M> remove(M model) throws UserIdEmptyException;

    @SneakyThrows
    Optional<M> findById(@Nullable String id);


    @SneakyThrows
    @NotNull
    Optional<M> removeById(@Nullable String id) throws AbstractException;


    @SneakyThrows
    void removeAll() throws UserIdEmptyException;
}
