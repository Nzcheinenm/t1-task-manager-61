package ru.t1.dkononov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.services.IPropertyService;
import ru.t1.dkononov.tm.api.services.IUserService;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.field.*;
import ru.t1.dkononov.tm.model.User;
import ru.t1.dkononov.tm.repository.model.ProjectRepository;
import ru.t1.dkononov.tm.repository.model.TaskRepository;
import ru.t1.dkononov.tm.repository.model.UserRepository;
import ru.t1.dkononov.tm.util.HashUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private UserRepository repository;

    @NotNull
    @Getter
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Getter
    @Autowired
    private ProjectRepository projectRepository;


    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user;
        user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @Nullable final User user;
        user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final User user;
        user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        if (role != null) user.setRole(role);
        repository.save(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findFirstByLogin(login).get();
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) throws EmailEmptyException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = repository.findFirstByEmail(email).get();
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User removeOne(@Nullable final User model) throws UserIdEmptyException {
        if (model == null) return null;
        @Nullable final User user;
        user = remove(model);
        if (user == null) return null;
        @Nullable final String userId = user.getId();
        getTaskRepository().deleteAllByUserId(userId);
        getProjectRepository().deleteAllByUserId(userId);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User removeByLogin(@Nullable final String login) throws AbstractFieldException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user;
        user = findByLogin(login);
        removeOne(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User removeByEmail(@Nullable final String email) throws AbstractFieldException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user;
        user = findByEmail(email);
        removeOne(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user;
        user = repository.findById(id).get();
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user;
        user = repository.findById(id).get();
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    @Nullable
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.findFirstByLogin(login) != null;
    }

    @Override
    @Nullable
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.findFirstByEmail(email) != null;
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public User lockUserByLogin(@Nullable final String login) throws LoginEmptyException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user;
        user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        return user;
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public User unlockUserByLogin(@Nullable String login) throws LoginEmptyException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user;
        user = findByLogin(login);
        user.setLocked(false);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).get();
    }

    @Override
    @Nullable
    public List<User> findAll(@Nullable Sort customSort) {
        return repository.findAll(customSort);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    @NotNull
    @SneakyThrows
    public Collection<User> set(@NotNull Collection<User> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::saveAndFlush);
        return models;
    }

}
