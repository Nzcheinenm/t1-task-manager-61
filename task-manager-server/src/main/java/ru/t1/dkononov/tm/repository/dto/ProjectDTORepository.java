package ru.t1.dkononov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.dto.model.ProjectDTO;
import ru.t1.dkononov.tm.enumerated.CustomSort;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> {

    @NotNull
    @Query("SELECT m FROM ProjectDTO m WHERE m.userId = :userId")
    List<ProjectDTO> findAllByUserId(@NotNull final String userId, @NotNull CustomSort customSort);

    @NotNull
    Optional<ProjectDTO> findFirstById(@NotNull final String id);

    @Query("SELECT m FROM ProjectDTO m WHERE m.userId = :userId")
    List<ProjectDTO> findByIndexForUserId(@NotNull final String userId, @NotNull final Pageable pageable);


    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    Optional<ProjectDTO> findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId);


}
