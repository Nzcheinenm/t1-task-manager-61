package ru.t1.dkononov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.dto.model.TaskDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> {

    @NotNull
    @Query("SELECT m FROM TaskDTO m WHERE m.userId = :userId")
    List<TaskDTO> findByIndexForUserId(@Nullable @Param("userId") String userId, @Nullable Pageable pageable);

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Optional<TaskDTO> findFirstByUserIdAndProjectIdAndId(@NotNull String userId, @NotNull String projectId, @NotNull String id);


    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    Optional<TaskDTO> findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId);

}
