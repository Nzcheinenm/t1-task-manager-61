package ru.t1.dkononov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface AbstractUserOwnedDTORepository<E extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<E> {


}
