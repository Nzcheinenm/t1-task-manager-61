package ru.t1.dkononov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.dto.model.ProjectDTO;
import ru.t1.dkononov.tm.enumerated.CustomSort;
import ru.t1.dkononov.tm.model.Project;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    @NotNull
    @Query("SELECT m FROM Project m WHERE m.user.id = :userId")
    List<Project> findAllByUserId(@NotNull final String userId);

    @NotNull
    @Query("SELECT m FROM Project m WHERE m.user.id = :userId")
    List<Project> findAllByUserId(@NotNull final String userId, @NotNull CustomSort customSort);

    @Query("SELECT m FROM Project m WHERE m.user.id = :userId")
    List<Project> findByIndexForUserId(@NotNull final String userId, @NotNull final Pageable pageable);

    @Nullable
    Optional<Project> findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteAllByUserId(@NotNull String userId);

}
