package ru.t1.dkononov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.api.services.IService;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.IdEmptyException;
import ru.t1.dkononov.tm.exception.field.IndexIncorrectException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.model.AbstractModel;
import ru.t1.dkononov.tm.repository.model.AbstractRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    @Autowired
    protected IConnectionService connectionService;

    @NotNull
    @Autowired
    private AbstractRepository<M> repository;

    @Override
    @SneakyThrows
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final M result;
        repository.save(model);
        result = findById(model.getId());
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Collection<M> add(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::save);
        return models;
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::saveAndFlush);
        return models;
    }


    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public M remove(@Nullable final M model) throws UserIdEmptyException {
        repository.delete(model);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).get();
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findByIndex(@Nullable final Integer index)
            throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findAll().get(0);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M result;
        result = repository.findById(id).get();
        remove(result);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M removeByIndex(@Nullable final Integer index) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final M result;
        result = repository.findAll().get(0);
        repository.delete(result);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll() throws UserIdEmptyException {
        repository.deleteAll();
    }

}
