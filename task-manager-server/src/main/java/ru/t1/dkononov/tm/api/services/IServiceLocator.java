package ru.t1.dkononov.tm.api.services;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkononov.tm.api.services.dto.*;

public interface IServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IUserDTOService getUserService();

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionDTOService getSessionService();

    @NotNull
    IAdminService getAdminService();

}
