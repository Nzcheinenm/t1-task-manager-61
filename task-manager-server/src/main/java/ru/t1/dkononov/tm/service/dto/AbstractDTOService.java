package ru.t1.dkononov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.api.services.dto.IDTOService;
import ru.t1.dkononov.tm.dto.model.AbstractModelDTO;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.IdEmptyException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.repository.dto.AbstractDTORepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO> implements IDTOService<M> {


    @NotNull
    @Autowired
    protected ApplicationContext context;


    @NotNull
    @Autowired
    protected IConnectionService connectionService;

    @NotNull
    @Autowired
    private AbstractDTORepository<M> repository;

    @Override
    @SneakyThrows
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Optional<M> add(@Nullable final M model) {
        if (model == null) throw new ProjectNotFoundException();
        final Optional<M> result;
        repository.save(model);
        result = findById(model.getId());
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Collection<M> add(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::save);
        return models;
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::save);
        return models;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Optional<M> remove(@Nullable final M model) throws UserIdEmptyException {
        repository.delete(model);
        return Optional.of(model);
    }

    @Override
    @SneakyThrows
    public Optional<M> findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Optional<M> removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> result;
        result = repository.findById(id);
        remove(result.get());
        return result;
    }


    @Override
    @SneakyThrows
    @Transactional
    public void removeAll() throws UserIdEmptyException {
        repository.deleteAll();
    }

}
