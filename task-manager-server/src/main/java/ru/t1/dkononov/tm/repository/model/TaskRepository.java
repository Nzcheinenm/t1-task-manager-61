package ru.t1.dkononov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.enumerated.CustomSort;
import ru.t1.dkononov.tm.model.Project;
import ru.t1.dkononov.tm.model.Task;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @NotNull
    @Query("SELECT m FROM Task m WHERE m.user.id = :userId")
    List<Task> findAllByUserId(@NotNull final String userId);

    @NotNull
    @Query("SELECT m FROM Task m WHERE m.user.id = :userId ")
    List<Task> findAllByUserId(@NotNull final String userId, @NotNull CustomSort customSort);


    @Query("SELECT m FROM Task m")
    List<Task> findByIndex(@NotNull final Pageable pageable);

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Query("SELECT m FROM Task m WHERE m.user.id = :userId")
    List<Task> findByIndexForUserId(@NotNull @Param("userId") final String userId, @NotNull final Pageable pageable);

    @Nullable
    Optional<Task> findByUserIdAndProjectIdAndId(@NotNull String userId, @NotNull String projectId, @NotNull String id);

    @Nullable
    Optional<Task> findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteAllByUserId(@NotNull String userId);

}