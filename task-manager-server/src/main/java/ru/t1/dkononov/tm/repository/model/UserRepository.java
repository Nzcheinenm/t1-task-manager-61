package ru.t1.dkononov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @Query("SELECT m FROM User m")
    List<User> findByIndex(@NotNull final Pageable pageable);

    @NotNull
    Optional<User> findFirstByLogin(@NotNull String login);

    @Nullable
    Optional<User> findFirstByEmail(@NotNull String email);

}
