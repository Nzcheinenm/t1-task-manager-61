package ru.t1.dkononov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.services.ISessionService;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.IdEmptyException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.model.Session;
import ru.t1.dkononov.tm.repository.model.SessionRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    @NotNull
    @Autowired
    private SessionRepository repository;


    @Nullable
    @Override
    @SneakyThrows
    public Session findById(@Nullable final String userId, @Nullable final String id)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session add(@Nullable final Session model) {
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final Session result;
        repository.save(model);
        result = repository.findById(model.getId()).get();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Collection<Session> add(@NotNull Collection<Session> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::save);
        return models;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session remove(@NotNull final String userId, @Nullable final Session model) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        repository.delete(model);
        return model;
    }

    @Override
    @Nullable
    public List<Session> findAll(@Nullable Sort customSort) {
        if (customSort == null) return findAll();
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id) != null;
    }

    @Override
    @NotNull
    public List<Session> findAll(@Nullable String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

}
