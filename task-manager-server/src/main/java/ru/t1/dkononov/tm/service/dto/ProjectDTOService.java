package ru.t1.dkononov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.services.dto.IProjectDTOService;
import ru.t1.dkononov.tm.dto.model.ProjectDTO;
import ru.t1.dkononov.tm.enumerated.Status;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.*;
import ru.t1.dkononov.tm.repository.dto.ProjectDTORepository;

import java.sql.SQLException;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectDTOService extends AbstractUserOwnedDTOService<ProjectDTO>
        implements IProjectDTOService {

    @NotNull
    @Autowired
    private ProjectDTORepository repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    )
            throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO result;
        result = new ProjectDTO();
        result.setUserId(userId);
        result.setName(name);
        result.setDescription(description);
        repository.save(result);
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name)
            throws AbstractFieldException, SQLException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setUserId(userId);
        repository.save(project);
        return project;
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    )
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project;
        project = repository.findFirstByUserIdAndId(userId, id).get();
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public ProjectDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    )
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project;
        project = repository.findByIndexForUserId(userId, PageRequest.of(index, 1)).get(0);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status
    )
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO project;
        project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public ProjectDTO changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @NotNull final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final ProjectDTO project;
        project = repository.findByIndexForUserId(userId, PageRequest.of(index, 1)).get(0);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @SneakyThrows
    public ProjectDTO findById(@NotNull String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        return repository.findFirstByUserIdAndId(userId, projectId).get();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO removeById(@NotNull String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO result;
        result = repository.findFirstByUserIdAndId(userId, projectId).get();
        remove(userId, result);
        return result;
    }

    public boolean existsById(@NotNull String userId, @NotNull String projectId) {
        return findById(userId, projectId) != null;
    }

    @Override
    public @NotNull ProjectDTO findOneById(@NotNull String userId, @NotNull String id) throws UserIdEmptyException, IdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findFirstById(id).get();
    }


}
