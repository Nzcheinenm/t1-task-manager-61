package ru.t1.dkononov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.dto.model.SessionDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> {

    @NotNull
    @Query("SELECT m FROM SessionDTO m WHERE m.userId = :userId")
    List<SessionDTO> findAllByUserId(@NotNull final String userId, @NotNull Sort customSort);


    @NotNull
    List<SessionDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    Optional<SessionDTO> findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId);

}
