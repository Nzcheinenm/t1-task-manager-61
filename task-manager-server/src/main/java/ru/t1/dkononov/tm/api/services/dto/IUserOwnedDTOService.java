package ru.t1.dkononov.tm.api.services.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkononov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.dkononov.tm.enumerated.CustomSort;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> {
    @SneakyThrows
    void clear(@Nullable final String userId) throws UserIdEmptyException;

    @SneakyThrows
    boolean existsById(@Nullable final String userId, @Nullable final String id) throws UserIdEmptyException;

    @NotNull
    @SneakyThrows
    List<M> findAll(@Nullable final String userId) throws UserIdEmptyException;

    @NotNull
    @SneakyThrows
    List<M> findAll();

    @Nullable
    @SneakyThrows
    List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) throws UserIdEmptyException;

    @NotNull
    @SneakyThrows
    List<M> findAll(
            @Nullable final String userId,
            @Nullable final CustomSort customSort
    ) throws UserIdEmptyException;

    @Nullable
    @SneakyThrows
    M add(@Nullable final String userId, @Nullable final M model)
            throws ProjectNotFoundException, UserIdEmptyException;

    @Nullable
    @SneakyThrows
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    @SneakyThrows
    Collection<M> set(@NotNull Collection<M> models);


    @Nullable
    @SneakyThrows
    M remove(@NotNull final String userId, @Nullable final M model) throws UserIdEmptyException;

    @Nullable
    @SneakyThrows
    M findById(@Nullable final String userId, @Nullable final String id)
            throws AbstractException;

    @Nullable
    @SneakyThrows
    M findByIndex(@Nullable final String userId, @Nullable final Integer index)
            throws AbstractException;

    @NotNull
    @SneakyThrows
    M removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    @SneakyThrows
    M removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractException;

    @SneakyThrows
    void removeAll(@Nullable final String userId) throws UserIdEmptyException;

}
