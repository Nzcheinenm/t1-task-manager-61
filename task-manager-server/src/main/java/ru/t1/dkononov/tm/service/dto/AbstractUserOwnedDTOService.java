package ru.t1.dkononov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.services.dto.IUserOwnedDTOService;
import ru.t1.dkononov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.dkononov.tm.enumerated.CustomSort;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.IdEmptyException;
import ru.t1.dkononov.tm.exception.field.IndexIncorrectException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.repository.dto.AbstractUserOwnedDTORepository;
import ru.t1.dkononov.tm.repository.dto.UserDTORepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTOService<M> implements IUserOwnedDTOService<M> {

    @NotNull
    @Autowired
    private AbstractUserOwnedDTORepository<M> repository;

    @NotNull
    @Getter
    @Autowired
    private UserDTORepository userRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.findAll().stream()
                .filter(x -> x.getUserId().equals(userId))
                .forEach(x -> repository.delete(x));
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll().stream()
                .filter(x -> x.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAll().stream()
                .filter(x -> x.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final CustomSort customSort
    ) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (customSort == null) return findAll(userId);
        return findAll(userId, customSort.getComparator());
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public M add(@Nullable final String userId, @Nullable final M model)
            throws ProjectNotFoundException, UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final M result;
        model.setUserId(userId);
        repository.save(model);
        result = findById(userId, model.getId());
        return result;
    }

    public @NotNull M findOneById(@NotNull String userId, @NotNull String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).get();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Collection<M> add(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::save);
        return models;
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::saveAndFlush);
        return models;
    }


    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public M remove(@NotNull final String userId, @Nullable final M model) throws UserIdEmptyException {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        repository.findAll().stream()
                .filter(x -> x.getUserId().equals(userId))
                .forEach(x -> repository.delete(x));
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findAll().stream()
                .filter(x -> x.getUserId().equals(userId)).findFirst().get();
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findAll().stream()
                .filter(x -> x.getUserId().equals(userId)).findFirst().get();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final @Nullable Optional<M> result;
        result = repository.findAll().stream()
                .filter(x -> x.getUserId().equals(userId)).findFirst();
        remove(userId, result.get());
        return result.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Optional<M> result;
        result = repository.findAll().stream()
                .filter(x -> x.getUserId().equals(userId)).findFirst();
        remove(result.get());
        return result.get();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.findAll().stream()
                .filter(x -> x.getUserId().equals(userId))
                .forEach(x -> repository.delete(x));
    }

}
