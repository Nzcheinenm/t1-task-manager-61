package ru.t1.dkononov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.services.dto.ISessionDTOService;
import ru.t1.dkononov.tm.dto.model.SessionDTO;
import ru.t1.dkononov.tm.enumerated.CustomSort;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.IdEmptyException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.repository.dto.SessionDTORepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO> implements ISessionDTOService {

    @NotNull
    @Autowired
    private SessionDTORepository repository;

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findById(@Nullable final String userId, @Nullable final String id)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll() {
        return repository.findAll();
    }


    @Override
    @SneakyThrows
    @Transactional
    public @Nullable Optional<SessionDTO> add(@Nullable final SessionDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        final @NotNull Optional<SessionDTO> result;
        repository.save(model);
        result = repository.findById(model.getId());
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Collection<SessionDTO> add(@NotNull Collection<SessionDTO> models) {
        if (models.isEmpty()) return Collections.emptyList();
        models.forEach(repository::save);
        return models;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO remove(@NotNull final String userId, @Nullable final SessionDTO model) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        repository.delete(model);
        return model;
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll(@Nullable CustomSort customSort) {
        if (customSort == null) return findAll();
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id) != null;
    }

    @Override
    @NotNull
    public List<SessionDTO> findAll(@Nullable String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

}
