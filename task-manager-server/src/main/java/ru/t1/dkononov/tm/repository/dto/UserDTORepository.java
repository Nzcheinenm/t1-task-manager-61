package ru.t1.dkononov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.dto.model.UserDTO;
import ru.t1.dkononov.tm.enumerated.CustomSort;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @NotNull
    @Query("SELECT m FROM UserDTO m")
    List<UserDTO> findByIndex(@NotNull final Pageable pageable);

    @Nullable
    Optional<UserDTO> findFirstByLogin(@NotNull String login);

    @Nullable
    Optional<UserDTO> findFirstByEmail(@NotNull String email);


}
