package ru.t1.dkononov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.services.*;
import ru.t1.dkononov.tm.api.services.dto.*;
import ru.t1.dkononov.tm.dto.model.UserDTO;
import ru.t1.dkononov.tm.endpoint.AbstractEndpoint;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.service.DomainService;
import ru.t1.dkononov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;


@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMAND = "ru.t1.dkononov.tm.command";

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @Getter
    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Getter
    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    @Autowired
    private IAdminService adminService;

    @Getter
    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;


    @NotNull
    @Autowired
    public static ApplicationContext context;

    @Getter
    @NotNull
    @Autowired
    private IUserDTOService userService;

    @Getter
    @NotNull
    @Autowired
    private IAuthService authService;

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = String.valueOf(propertyService.getServerPort());
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        try {
            //initDemoData();
            initEndpoints();
            initLogger();
            initPID();
        } catch (final Exception e) {
            loggerService.error(e);
            System.err.println("[INIT FAIL]");
        }
    }

    @SneakyThrows
    public void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }


    private void stop() {
        loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN **");
    }

    private void initPID() throws IOException {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() throws Exception {
        @NotNull final UserDTO test = userService.create("test", "test", "test@test.ru");
        @NotNull final UserDTO user = userService.create("user", "user", "user@test.ru");
        @NotNull final UserDTO admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(test.getId(), "Jira", "Desc");
        projectService.create(test.getId(), "Confluence", "Conf");
        projectService.create(admin.getId(), "SoapUI", "Pelp");
        projectService.create(user.getId(), "Postman", "Ferst");

        taskService.create(test.getId(), "Work", "Working");
        taskService.create(admin.getId(), "Homework", "Speed");
    }


    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER SERVER **");
        loggerService.initJmsLogger();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

}
