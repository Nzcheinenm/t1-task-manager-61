package ru.t1.dkononov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.model.Session;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

    @Query("SELECT m FROM Session m")
    List<Session> findByIndex(@Nullable Pageable pageable);

    @NotNull
    @Query("SELECT m FROM Session m WHERE m.user.id = :userId")
    List<Session> findAllByUserId(@NotNull final String userId);

    @Nullable
    Optional<Session> findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteAllByUserId(@NotNull String userId);

}
