package ru.t1.dkononov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.model.AbstractModel;

import java.util.List;
import java.util.Optional;


@Repository
@Scope("prototype")
public interface AbstractRepository<E extends AbstractModel> extends JpaRepository<E, String> {

}
