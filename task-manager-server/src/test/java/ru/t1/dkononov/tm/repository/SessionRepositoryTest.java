package ru.t1.dkononov.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.marker.DataCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.repository.dto.SessionDTORepository;
import ru.t1.dkononov.tm.repository.dto.UserDTORepository;
import ru.t1.dkononov.tm.repository.model.SessionRepository;
import ru.t1.dkononov.tm.service.PropertyService;

import static ru.t1.dkononov.tm.constant.TestData.SESSION;
import static ru.t1.dkononov.tm.constant.TestData.USER1;

@Category(DataCategory.class)
public class SessionRepositoryTest extends AbstractSchemaTest {


    @NotNull
    private final SessionDTORepository repository = context.getBean(SessionDTORepository.class);

    @BeforeClass
    public void setup() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
    }

    @Before
    public void init() {
        try {
            @NotNull final UserDTORepository userDTORepository = context.getBean(UserDTORepository.class);
            entityManager.getTransaction().begin();
            userDTORepository.save(USER1);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() {
        try {
            @NotNull final SessionDTORepository repository = context.getBean(SessionDTORepository.class);
            entityManager.getTransaction().begin();
            repository.deleteByUserId(USER1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void addByUserId() throws UserIdEmptyException, ProjectNotFoundException {
        try {
            @NotNull final SessionDTORepository repository = context.getBean(SessionDTORepository.class);
            repository.save(SESSION);
            Assert.assertNotNull(repository.findById(SESSION.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() {
        try {
            @NotNull final SessionDTORepository repository = context.getBean(SessionDTORepository.class);
            Assert.assertEquals(SESSION.getUserId(), USER1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAllNull() {
        try {
            @NotNull final SessionRepository sessionRepository = context.getBean(SessionRepository.class);
            Assert.assertTrue(sessionRepository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findByNullId() {
        try {
            @NotNull final SessionDTORepository repository = context.getBean(SessionDTORepository.class);
            Assert.assertNull(repository.findFirstByUserIdAndId(USER1.getId(), null));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
