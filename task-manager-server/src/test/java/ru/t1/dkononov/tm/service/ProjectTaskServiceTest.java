package ru.t1.dkononov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.dkononov.tm.api.services.dto.IProjectDTOService;
import ru.t1.dkononov.tm.api.services.dto.ITaskDTOService;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.marker.UnitCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.repository.dto.ProjectDTORepository;
import ru.t1.dkononov.tm.repository.dto.TaskDTORepository;
import ru.t1.dkononov.tm.service.dto.ProjectTaskDTOService;

import java.util.Objects;

import static ru.t1.dkononov.tm.constant.TestData.*;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest extends AbstractSchemaTest {


    @Nullable
    private final ProjectDTORepository projectRepository = context.getBean(ProjectDTORepository.class);

    @Nullable
    private final TaskDTORepository taskRepository = context.getBean(TaskDTORepository.class);

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ProjectTaskDTOService projectTaskService;

    @Before
    public void before() throws UserIdEmptyException, ProjectNotFoundException, LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");


        projectRepository.save(USER_PROJECT);
        projectRepository.save(USER_PROJECT2);

        taskRepository.save(USER_TASK);
        taskRepository.save(USER_TASK2);
    }

    @After
    public void after() throws UserIdEmptyException {
        projectRepository.deleteByUserId(USER1.getId());
        taskRepository.deleteByUserId(USER1.getId());

    }

    @Test
    public void bindTaskToProject() throws Exception {
        projectTaskService.bindTaskToProject(USER1.getId(), USER_PROJECT.getId(), USER_TASK.getId());
        Assert.assertTrue(Objects.equals(USER_TASK.getProjectId(), USER_PROJECT.getId()));
    }

    @Test
    public void removeProjectById() throws Exception {
        projectTaskService.removeProjectById(USER1.getId(), USER_PROJECT.getId());
        Assert.assertFalse(projectRepository.findFirstById(USER_PROJECT.getId()) != null);
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER_PROJECT.getId(), USER_TASK.getId());
        Assert.assertNull(USER_TASK.getProjectId());
    }

}
