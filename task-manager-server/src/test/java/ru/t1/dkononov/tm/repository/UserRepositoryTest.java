package ru.t1.dkononov.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.dkononov.tm.dto.model.UserDTO;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.field.AbstractFieldException;
import ru.t1.dkononov.tm.exception.field.LoginEmptyException;
import ru.t1.dkononov.tm.marker.DataCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.repository.dto.ProjectDTORepository;
import ru.t1.dkononov.tm.repository.dto.UserDTORepository;
import ru.t1.dkononov.tm.service.PropertyService;
import ru.t1.dkononov.tm.util.HashUtil;

import static ru.t1.dkononov.tm.constant.TestData.*;


@Category(DataCategory.class)
public class UserRepositoryTest extends AbstractSchemaTest {

    @NotNull
    @Autowired
    private PropertyService propertyService;


    @NotNull
    private final ProjectDTORepository projectRepository = context.getBean(ProjectDTORepository.class);


    @NotNull
    private final UserDTORepository repository = context.getBean(UserDTORepository.class);

    @NotNull
    private static final String LOGIN_TEST = "logintest";

    @NotNull
    private static final String PASS_TEST = "logintest";

    @NotNull
    private static final String PASS_RETEST = "logintest";

    @NotNull
    private static final String NAME = "firstName";

    @NotNull
    private UserDTO userTesting;

    @Before
    public void before() throws AbstractException, LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        try {
            @NotNull final UserDTORepository repository = context.getBean(UserDTORepository.class);
            repository.save(USER1);
            repository.save(USER2);
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin(LOGIN_TEST);
            user.setPasswordHash(HashUtil.salt(propertyService, PASS_TEST));
            user.setRole(Role.USUAL);
            repository.save(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() {
        try {
            @NotNull final UserDTORepository repository = context.getBean(UserDTORepository.class);
            entityManager.getTransaction().begin();
            repository.deleteAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void create() throws AbstractException {
        try {
            @NotNull final UserDTORepository repository = context.getBean(UserDTORepository.class);
            @NotNull final UserDTO user1 = new UserDTO();
            user1.setLogin(LOGIN);
            user1.setPasswordHash(HashUtil.salt(propertyService, PASSWORD));
            user1.setRole(Role.USUAL);
            repository.save(user1);
            @Nullable final UserDTO user = repository.findFirstByLogin(LOGIN).get();
            Assert.assertNotNull(user);
            projectRepository.save(USER_PROJECT);
            Assert.assertNotNull(projectRepository.findFirstById(USER_PROJECT.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findByLogin() throws LoginEmptyException {
        try {
            @NotNull final UserDTORepository repository = context.getBean(UserDTORepository.class);
            Assert.assertNotNull(repository.findFirstByLogin(LOGIN_TEST));
            entityManager.close();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Test
    public void removeById() throws AbstractFieldException {
        try {
            @NotNull final UserDTORepository repository = context.getBean(UserDTORepository.class);
            repository.deleteById(userTesting.getId());
            Assert.assertNull(repository.findFirstByLogin(LOGIN_TEST));
            entityManager.close();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
