package ru.t1.dkononov.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.dkononov.tm.marker.DataCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.repository.dto.TaskDTORepository;
import ru.t1.dkononov.tm.repository.dto.UserDTORepository;
import ru.t1.dkononov.tm.service.PropertyService;

import static ru.t1.dkononov.tm.constant.TestData.*;

@Category(DataCategory.class)
public class TaskRepositoryTest extends AbstractSchemaTest {

    @NotNull
    @Autowired
    private final TaskDTORepository repository = context.getBean(TaskDTORepository.class);

    @BeforeClass
    public static void before() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
    }

    @Before
    public void init() {
        try {
            @NotNull final UserDTORepository userDTORepository = context.getBean(UserDTORepository.class);
            entityManager.getTransaction().begin();
            userDTORepository.save(USER1);
            repository.save(USER_TASK);
            repository.save(USER_TASK2);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() {
        try {
            @NotNull final TaskDTORepository repository = context.getBean(TaskDTORepository.class);
            entityManager.getTransaction().begin();
            repository.deleteByUserId(USER1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() {
        try {
            @NotNull final TaskDTORepository repository = context.getBean(TaskDTORepository.class);
            entityManager.getTransaction().begin();
            repository.save(USER_TASK);
            Assert.assertEquals(USER_TASK.getUserId(), USER1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAll() {
        try {
            @NotNull final TaskDTORepository repository = context.getBean(TaskDTORepository.class);
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.save(USER_TASK);
            Assert.assertEquals(USER_TASK, repository.findById(USER_TASK.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findById() {
        try {
            @NotNull final TaskDTORepository repository = context.getBean(TaskDTORepository.class);
            Assert.assertNotNull(repository.findFirstByUserIdAndId(USER1.getId(), USER_TASK.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeAll() {
        try {
            @NotNull final TaskDTORepository repository = context.getBean(TaskDTORepository.class);
            repository.deleteAll();
            Assert.assertTrue(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
